$(function(){
     $('#header').load('./shared/header.html',()=>{
          $('.dropdown-toggle').click(function(e){
               var parent = $(this).parent();
               if(parent.hasClass('show')) { 
                $(this).attr("aria-expanded","false");
                 parent.removeClass('show'); 
                 parent.find('.row.dropdown-menu').removeClass('show'); 
               } else {
                initDropdown();
                 $(this).attr("aria-expanded","true");
                  parent.addClass('show');
                  parent.find('.row.dropdown-menu').addClass('show');
               }
               e.preventDefault();

           });
         

     });
     $('#footer').load('./shared/footer.html');
	 $('#contactform').load('./shared/contactform.html');
     $(document).on("click", function(event){
          var $trigger = $(".dropdown");
          if($trigger !== event.target && !$trigger.has(event.target).length){
               initDropdown();
          }            
      });
    
  function initDropdown(){
     $('.dropdown-toggle').parent().find('.row.dropdown-menu').removeClass('show');
     $('.dropdown-toggle').parent().removeClass('show');
     $('.dropdown-toggle').attr("aria-expanded","false");
  }
});